<?php 

require 'vendor/autoload.php';

use GuzzleHttp\Client;
// use Guzzle\Http\Exception\ClientErrorResponseException;

$client = new Client([
    // Base URI is used with relative requests
    'base_uri' => 'http://local.oauth-server.com',[
        // 'proxy' => 'http://testclient:testpass@local.oauth-server.com'
    ]
    // You can set any number of default request options.
]);
$contents = '';
$code = 0;
try {
    $response = $client->request('POST', '/token.php',[
        'headers' => [
            'cache-control' => 'no-cache',
            'Content-Type' => 'application/x-www-form-urlencoded'
        ],
        'form_params' => [
            // 'access_token' => '50c364df65d612ab3f8dde98d279c603e5310681'
            'grant_type' =>'client_credentials',
            'client_id' => 'simpel1',
            'client_secret' => 'simpel1',
            // 'redirect_uri' => 'http://local.oauth-client.com/home.php'
        ]
    ]);

    $contents = json_decode((string) $response->getBody(), true);
    $code = json_decode((string) $response->getStatusCode(), true);
} catch (Exception $exception) {
    $code = json_decode((string) $exception->getResponse()->getStatusCode(), true);
    $contents = json_decode((string) $exception->getResponse()->getBody(), true);
    // $contents = $exception->getResponse()->getBody(true);
}
echo '<pre>';
// print_r($code);
print_r($contents);
echo '</pre>';
die();