<?php 

function getResource($client, $token){
    $contents = '';
    $code = 0;
    try {
        $response = $client->request('POST', '/resource.php',[
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'access_token' => $token,
        		// 'scope' => 'http://local.simpel.com'
                // 'redirect_uri' => 'http://local.oauth-client.com/home.php'
            ]
        ]);

        $contents = json_decode((string) $response->getBody(), true);
        $code = json_decode((string) $response->getStatusCode(), true);
    } catch (Exception $exception) {
        $code = json_decode((string) $exception->getResponse()->getStatusCode(), true);
        $contents = json_decode((string) $exception->getResponse()->getBody(), true);
        // $contents = $exception->getResponse()->getBody(true);
    }
    
    $res = [
    	'code' => $code,
    	'contents' => $contents
    ];

    return $res;
}

function getGetNewToken($client, $client_id, $client_secret){
    $contents = '';
    $code = 0;
    try {
	    $response = $client->request('POST', '/token.php',[
	        'headers' => [
	            'cache-control' => 'no-cache',
	            'Content-Type' => 'application/x-www-form-urlencoded'
	        ],
	        'form_params' => [
	            // 'access_token' => '50c364df65d612ab3f8dde98d279c603e5310681'
	            'grant_type' =>'client_credentials',
	            'client_id' => $client_id,
	            'client_secret' => $client_secret,
	            // 'redirect_uri' => 'http://local.oauth-client.com/home.php'
	        ]
	    ]);

	    $contents = json_decode((string) $response->getBody(), true);
	    $code = json_decode((string) $response->getStatusCode(), true);
	} catch (Exception $exception) {
	    $code = json_decode((string) $exception->getResponse()->getStatusCode(), true);
	    $contents = json_decode((string) $exception->getResponse()->getBody(), true);
	    // $contents = $exception->getResponse()->getBody(true);
	}
    $res = [
    	'code' => $code,
    	'contents' => $contents
    ];
    return $res;
}

function getGetNewTokenPassword($client, $client_id, $client_secret, $username, $password){
    $contents = '';
    $code = 0;
    try {
	    $response = $client->request('POST', '/token.php',[
	        'headers' => [
	            'cache-control' => 'no-cache',
	            'Content-Type' => 'application/x-www-form-urlencoded'
	        ],
	        'form_params' => [
	            // 'access_token' => '50c364df65d612ab3f8dde98d279c603e5310681'
	            'grant_type' =>'password',
	            'client_id' => $client_id,
	            'client_secret' => $client_secret,
	            'username' => $username,
	            'password' => $password
	            // 'redirect_uri' => 'http://local.oauth-client.com/home.php'
	        ]
	    ]);

	    $contents = json_decode((string) $response->getBody(), true);
	    $code = json_decode((string) $response->getStatusCode(), true);
	} catch (Exception $exception) {
	    $code = json_decode((string) $exception->getResponse()->getStatusCode(), true);
	    $contents = json_decode((string) $exception->getResponse()->getBody(), true);
	    // $contents = $exception->getResponse()->getBody(true);
	}
    $res = [
    	'code' => $code,
    	'contents' => $contents
    ];
    return $res;
}