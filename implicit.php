<?php 

require 'vendor/autoload.php';

use GuzzleHttp\Client;
// use Guzzle\Http\Exception\ClientErrorResponseException;

$client = new Client([
    // Base URI is used with relative requests
    'base_uri' => 'http://local.oauth-server.com',[
        // 'proxy' => 'http://testclient:testpass@local.oauth-server.com'
    ]
    // You can set any number of default request options.
]);
$contents = '';
$code = 0;
try {
    $response = $client->request('POST', '/authorize.php',[
        'headers' => [
            'cache-control' => 'no-cache',
            'Content-Type' => 'application/x-www-form-urlencoded'
        ],
        'form_params' => [
            'access_token' => '0c4bb8909466e059c2ae0281838ec14cebed9171',
            'response_type' => 'token',
            'client_id' => 'TestClient',
            'redirect_uri' => 'http://local.oauth-client.com/home.php'
        ]
    ]);

    $contents = json_decode((string) $response->getBody(), true);
    // $json = $response->json();
    print_r($contents);
    exit;
    $code = json_decode((string) $response->getStatusCode(), true);
} catch (Exception $exception) {
    $code = json_decode((string) $exception->getResponse()->getStatusCode(), true);
    $contents = json_decode((string) $exception->getResponse()->getBody(), true);
    // $contents = $exception->getResponse()->getBody(true);
}
echo '<pre>';
// print_r($code);
print_r($contents);
echo '</pre>';
die();